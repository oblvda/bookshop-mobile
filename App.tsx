import 'react-native-gesture-handler';
import { AuthProvider } from './src/auth/auth-context';
import Navigation from './src/components/Navigation';

export default function App() {

  return (
    <>
      <AuthProvider>
        <Navigation />
      </AuthProvider>
    </>
  );
}
