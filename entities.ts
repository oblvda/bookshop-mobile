export interface Author {
    id?: number;
    name?: string;
    lastname?: string;
    birthdate?: Date | string;
};

export interface Genre {
    id?: number;
    genreLabel?: string;
}

export interface User {
    id?: number;
    firstname?: string;
    lastname?: string; 
    email?: string; 
    bookshop?: string;
    admin?: User;
    password?: string; 
    role?: string
}

export interface Order {
    id?: number;
    receptionDate?: Date | string;
    bookQuantity?: number;
    total?: number;
    comment?: string;
    book?: Book;
    idUser?:number;
    idBook?:number;
    user?: User;
}

export interface Publisher {
    id?: number;
    publisherLabel?: string;
}

export interface Book {
    id?: number;
    title?: string;
    isbn?: string;
    publicationDate?: Date | string;
    comment?: string;
    stock?: number;
    isAvailable?: boolean;
    price?: number;
    idGenre?: number;
    idAuthor?: number;
    idPublisher?: number;
    genre?: Genre;
    author?: Author;
    publisher?: Publisher;
    order?: Order;
}

export interface Sale {
    id?: number; 
    saleDate?: Date | string; 
    bookQuantity?: number;
    book: Book;
    idUser?:number;
    idBook?:number;
    user?: User;
}