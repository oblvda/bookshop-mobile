import React, { createContext, useState, useEffect, useContext, ReactNode } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

interface AuthContextType {
    token: string | null;
    loading: boolean;
    setToken: (token: string | null) => void;
    updateToken: (newToken: string) => void;
    clearToken: () => void;
}

const AuthContext = createContext<AuthContextType>({
    token: null,
    loading: true,
    setToken: () => { },
    updateToken: () => { },
    clearToken: () => { },
});

interface AuthProviderProps {
    children: ReactNode;
}

export const AuthProvider: React.FC<AuthProviderProps> = ({ children }) => {
    const [token, setToken] = useState<string | null>(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchToken = async () => {
            try {
                const storedToken = await AsyncStorage.getItem('authToken');
                setToken(storedToken);
            } catch (error) {
                console.error('Error fetching token:', error);
            } finally {
                setLoading(false);
            }
        };
        fetchToken();
    }, []);

    const updateToken = async (newToken: string) => {
        try {
            await AsyncStorage.setItem('authToken', newToken);
            setToken(newToken);
        } catch (error) {
            console.error('Error updating token:', error);
        }
    };

    const clearToken = async () => {
        try {
            await AsyncStorage.removeItem('authToken');
            setToken(null);
        } catch (error) {
            console.error('Error clearing token:', error);
        }
    };

    return (
        <AuthContext.Provider
            value={{
                token,
                loading,
                setToken,
                updateToken,
                clearToken,
            }}>
            {children}
        </AuthContext.Provider>
    );
};

export const useAuth = () => useContext(AuthContext);

export default AuthProvider;