import axios from "axios";

export async function postLogin(email?: string, password?: string) {
    const response = await axios.post<{ token: string }>('http://10.0.20.31:8000/api/login', { email, password });
    return response.data;
}