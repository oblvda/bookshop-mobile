const colors = {
  primary: "#3100A2",
  light: "#3100A2",
  dark: "#220071",
};

export default colors;