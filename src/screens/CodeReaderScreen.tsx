import React from 'react'
import { Image, Text, View } from 'react-native';
import CodeReaderForm from '../components/CodeReaderForm';

export default function CodeReaderScreen({ navigation }: any) {
  return (
    <>
      <View
        className='flex flex-col w-screen h-screen'
      >
        <View
          className='h-1/4 w-screen flex justify-center items-center bg-[#3100A2]'
        >
          <Image
            source={require('../../assets/open-book.png')}
            className='object-scale-down h-28 w-28'
          >
          </Image>
        </View>
        <View
          className='flex items-center justify-center text-center'
        >
          <Text
            className='text-2xl text-center my-10 text-[#3100A2]'
          >
            {`Lecteur de code`}
          </Text>
          <View>
            <CodeReaderForm navigation={navigation} />
          </View>
        </View>
      </View>
    </>
  )
}
