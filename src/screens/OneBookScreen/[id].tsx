import React, { useEffect, useState } from 'react'
import { Image, Text, View } from 'react-native';
import { fetchOneBook } from '../../services/book-service';
import { Book } from '../../../entities';
import { useAuth } from '../../auth/auth-context';
import dayjs from 'dayjs';
import { useRoute } from '@react-navigation/native';

export default function OneBookScreen({ navigation }: any) {
    const route = useRoute();
    const { token } = useAuth();
    const { id }: any = route.params;
    const [book, setBook] = useState<Book>();

    useEffect(() => {
        fetchOneBook(id, token)
            .then(data => setBook(data))
            .catch(error => {
                if (error.response === 404) {
                    navigation.navigate('404');
                } else if (error.response === 401 || token === null || token === undefined) {
                    navigation.navigate('Login');
                }
            });
    }, [id]);

    return (
        <>
            <View
                className='h-1/4 w-screen flex justify-center items-center bg-[#3100A2]'
            >
                <Image
                    source={require('../../../assets/open-book.png')}
                    className='object-scale-down h-28 w-28'
                >
                </Image>
            </View>
            <View
                className='justify-center'
            >
                <Text
                    className='text-2xl text-center my-10 text-[#3100A2]'
                >
                    {`Informations du livre`}
                </Text>
            </View>
            <View>
                <View
                    className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                >
                    <Text
                        className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                    >
                        {`Titre du livre: `}
                    </Text>
                    <Text>
                        {book?.title}
                    </Text>
                </View>
                <View
                    className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                >
                    <Text
                        className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                    >
                        {`Date de publication: `}
                    </Text>
                    <Text>
                        {dayjs(book?.publicationDate).format("YYYY")}
                    </Text>
                </View>
                <View
                    className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                >
                    <Text
                        className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                    >
                        {`Stock: `}
                    </Text>
                    <Text>
                        {book?.stock}
                    </Text>
                </View>
                <View
                    className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                >
                    <Text
                        className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                    >

                        {`Genre: `}
                    </Text>
                    <Text >
                        {book?.genre?.genreLabel}
                    </Text>
                </View>
                <View
                    className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                >
                    <Text
                        className='justify-center flex-wrap flex flex-row text-[#3100A2]'>
                        {`Auteur: `}
                    </Text>
                    <Text>
                        {book?.author?.name} {book?.author?.lastname}
                    </Text>
                </View>
                <View
                    className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                >
                    <Text
                        className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                    >
                        {`Maison d'édition: `}
                    </Text>
                    <Text>
                        {book?.publisher?.publisherLabel}
                    </Text>
                </View>
                <View
                    className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                >
                    <Text
                        className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                    >
                        {`ISBN: `}
                    </Text>
                    <Text>
                        {book?.isbn}
                    </Text>
                </View>
                <View
                    className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                >
                    <Text
                        className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                    >
                        {`Prix: `}
                    </Text>
                    <Text>
                        {book?.price}€
                    </Text>
                </View>
                <View
                    className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                >
                    <Text
                        className='justify-center flex-wrap flex flex-row text-[#3100A2]'
                    >
                        {`Commentaire: `}
                    </Text>
                    <Text>
                        {book?.comment}
                    </Text>
                </View>
            </View>
        </>
    )
}
