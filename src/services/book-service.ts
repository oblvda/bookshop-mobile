import axios from "axios";
import { Book } from "../../entities";

export async function findBook(book: Book, token: string | null) {
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        }
    };
    const response = await axios.get<Book>(`http://10.0.20.31:8000/api/book/isbn/${book.isbn}`, config);
    return response.data;
}

export async function fetchOneBook(id: string, token: string | null) {
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        }
    };
    const response = await axios.get<Book>(`http://10.0.20.31:8000/api/book/${id}`, config);
    return response.data;
}