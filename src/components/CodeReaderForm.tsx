import React, { useState } from 'react'
import { View, TextInput, TouchableOpacity, Text } from 'react-native';
import { Book } from '../../entities';
import { findBook } from '../services/book-service';
import { useAuth } from '../auth/auth-context';

export default function CodeReaderForm({ navigation }: any) {
    const [book, setBook] = useState<Book>({ isbn: '' });
    const [error, setError] = useState('');
    const { token } = useAuth();

    const handleChange = (name: string, value: string) => {
        setBook({
            ...book,
            [name]: value
        });
    };

    const handleSubmit = async () => {
        try {
            const foundBook = await findBook(book, token);
            setBook(foundBook);
            console.log('Found Book:', foundBook); 
            console.log('Book ID:', foundBook.id);
            navigation.navigate('Page livre', { id: foundBook.id });
        } catch (error: any) {
            if (error.response === 404)
                setError("Nous n'avons pas réussi à trouver votre livre. Réessayez avec un autre code.");
        }
    };

    return (
        <>
            {error &&
                <Text
                    className='text-red-500'
                >
                    {error}
                </Text>}
            <View
                className='flex flex-col items-center justify-center text-center'
            >
                <TextInput
                    value={book.isbn}
                    placeholder="Code ISBN"
                    onChangeText={(value) => handleChange('isbn', value)}
                    className="appearance-none border border-[#CECECE] w-64 h-16 p-4 m-4 text-[#CECECE] bg-[#EEEEEE] leading-tight focus:outline-none focus:shadow-outline"
                />
                <TouchableOpacity
                    className='h-16 bg-[#3100A2] m-4 w-64 flex justify-center items-center'
                    onPress={handleSubmit}
                >
                    <Text
                        className='text-white text-center'
                    >
                        {`Rechercher`}
                    </Text>
                </TouchableOpacity>
            </View>
        </>
    )
}
