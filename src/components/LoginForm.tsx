import React, { useState } from 'react'
import { postLogin } from '../auth/auth-service';
import { User } from '../../entities';
import { useAuth } from '../auth/auth-context';
import { TouchableOpacity, Text, TextInput, View } from 'react-native';

export default function LoginForm({ navigation }: any) {
  const { setToken } = useAuth();

  const [user, setUser] = useState<User>({
    email: '',
    password: ''
  });
  const [error, setError] = useState('');

  const handleSubmit = async () => {
    try {
      const response = await postLogin(user.email, user.password);
      if (response.token) {
        setToken(response.token);
        navigation.navigate("Lecteur de code");
      }
    } catch (error) {
      setError('Email ou mot de passe invalide.');
    }
  };

  const handleChange = (name: string, value: string) => {
    setUser({
      ...user,
      [name]: value
    });
  };


  return (
    <>
      {error &&
        <Text
          className='text-red-500'
        >
          {error}
        </Text>}
      <View
        className='flex flex-col items-center justify-center text-center'
      >
        <TextInput
          value={user.email}
          placeholder="Email"
          inputMode='email'
          onChangeText={(value) => handleChange('email', value)}
          className="appearance-none border border-[#CECECE] w-64 h-16 p-4 m-4 text-[#CECECE] bg-[#EEEEEE] leading-tight focus:outline-none focus:shadow-outline"
        />
        <TextInput
          value={user.password}
          placeholder="Mot de passe"
          onChangeText={(value) => handleChange('password', value)}
          secureTextEntry
          className="appearance-none border border-[#CECECE] w-64 h-16 p-4 m-4 text-[#CECECE] bg-[#EEEEEE] leading-tight focus:outline-none focus:shadow-outline"
        />
        <TouchableOpacity
          className='h-16 bg-[#3100A2] m-4 w-64 flex justify-center items-center'
          onPress={handleSubmit}
        >
          <Text
            className='text-white text-center'
          >
            {`Se connecter`}
          </Text>
        </TouchableOpacity>
      </View>
    </>
  )
}
