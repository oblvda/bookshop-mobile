import 'react-native-gesture-handler';
import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import LoginScreen from "../screens/LoginScreen";
import CodeReaderScreen from "../screens/CodeReaderScreen";
import OneBookScreen from '../screens/OneBookScreen/[id]';

const Stack = createStackNavigator();

export default function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={LoginScreen}
        />
        <Stack.Screen
          name="Lecteur de code"
          component={CodeReaderScreen}
        />
        <Stack.Screen
          name={"Page livre"}
          component={OneBookScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
